---
layout: post
title: "【直播报名】教你使用 Gitee Go 构造高效流水线"
---

<p>Gitee自研 CI/CD 工具 Gitee Go 正式上线后，我们收到了许多用户的反馈。</p>

<p>经过近期不断地改进，现在我们把它介绍给更多 Gitee 用户。</p>
