---
layout: post
title: "码云组织改版啦，老页面我实在看不下去了！！！"
---

<p>码云老的组织页面太丑了，于是乎我们想要改改。于是乎在新任产品经理的主导下，我们是改了 N 个版本力求一个至少团队自己觉得还凑合的结果。前前后后折腾了两三个月，产品经理是遍体鳞伤，被工程师打得他女朋友都不认得他了（设计部同事表示这锅他们不背）。</p><p>我只能借用郭德纲的一句话对产品经理说 —— 你就是干这个的，吃这碗饭的，被打是很正常的事。</p><p>我们内部是打完了，接下来就把组织页面放出来，让你们接着打：</p><p><img src="https://oscimg.oschina.net/oscnet/8f280d1fa12d4a68815132b48a6a53e1f06.jpg" width="800" height="663"/></p><p>------ 华丽分割线 ------</p><p>码云组织主要用于开源团队协作开发，组织成员不限，但是总的私有项目成员数（包括管理员）不超过 5 人。私有项目成员超过 5 人请使用企业版。</p><p>然后请容许我替产品经理跟大家求个情，请大家下手时不要太狠，留一条命好让他继续干活，虽然他视死如归。</p><p><img src="https://oscimg.oschina.net/oscnet/0a72e264122637af20dd4c3fde74c188661.jpg" width="400"/></p><p><a href="https://gitee.com/" _src="https://gitee.com/">https://gitee.com/</a></p>