---
layout: post
title: "码云 SSH 接入近期更新汇总"
---

**新增（调整）功能**

+   码云 SSH 接入已经支持 Git Wire Protocol，可以使用如下命令体验 v2 协议：

>GIT_TRACE=2 GIT_TRACE_PACKET=1  GIT_TRANSFER_TRACE=1 git -c protocol.version=2 ls-remote

+   SSH 服务器改进了错误输出，当用户通过 SSH 访问某存储库失败时，不再是简单的 `Access Denied.`，而是输出详细信息：

![](https://images.gitee.com/uploads/images/2018/0802/151230_85f85b85_1034.png)

+   码云调整 `ssh -T git@gitee.com` 和 `command not found` 的输出，与 Github 一致，如下图：

![](https://images.gitee.com/uploads/images/2018/0802/151632_093a6366_1034.png)

**缺陷修复**

码云 SSH 目前已经修正了 ECDSA Hostkey 的支持，目前支持的类型为 `ecdsa-sha2-nistp256`,`ecdsa-sha2-nistp384`,`ecdsa-sha2-nistp521`，在生成 ECDSA 的时候，如果前缀是 ssh-ecdsa 或者 ecdsa 而不是上述三种，则不支持。
