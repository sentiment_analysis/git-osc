---
layout: post
title: "码云全面切换到 gitee.com 独立域名的公告"
---

<p>码云启用 gitee.com 独立域名已经快一年时间了，这段时间内 git.oschina.net 和 gitee.com 两个域名都可以使用。我们计划在未来一周内全面切换到 gitee.com 域名上，对此可能造成的影响是推送代码会报错，请将仓库地址中的&nbsp;git.oschina.net 替换成 gitee.com 并全面使用 https&nbsp;即可。</p><p>另外你已经在分发项目地址的时候请使用 gitee.com 地址，以免造成用户困扰。开源项目的作者在涉及一些使用文档时也请将 git.oschina.net 替换成 gitee.com 。</p><p>对此造成您不必要的麻烦，敬请谅解。</p>