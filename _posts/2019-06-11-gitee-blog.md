---
layout: post
title: "坑爹代码 | Stream 玩得最 6 的代码，看过的人都惊呆了"
---

<p>Stream 作为 Java 8 的一大亮点，它与 java.io 包里的 InputStream 和 OutputStream 是完全不同的概念。Java 8 中的 Stream 是对集合（Collection）对象功能的增强，它专注于对集合对象进行各种非常便利、高效的聚合操作（aggregate operation），或者大批量数据操作 (bulk data operation)。Stream API 借助于同样新出现的 Lambda 表达式，极大的提高编程效率和程序可读性。</p>

<p><img alt="图 1.  流管道 (Stream Pipeline) 的构成" height="284" src="https://static.oschina.net/uploads/img/201906/11072804_N6we.png" width="331" /></p>

<p>那么 Java 8 的 Stream 到底是王者，还是个巨坑，这完全取决于你是怎么玩的？</p>

<p>我不得不说，下面代码是我从业 20 年（说谁呢，谁从业 20 年，我今年 19&nbsp; 岁！！！）看过最牛逼的 Stream 的用法：</p>

<pre>
<code class="language-java">Optional.of(req)
        .map(e -&gt; e.clueUid)
        .map(id -&gt; {
            final ClueExample example = new ClueExample();
            example.createCriteria()
                    .andClueUidEqualTo(id)
                    .andDeletedEqualTo(false)
                    .andReceivedEqualTo(false)
                    .andCreateTimeGreaterThan(now - cluetime);
            example.setOrderByClause("create_time asc");
            return example;
        })  // 获取该被邀请人所有未过期且未被领取的线索的线索
        .map(clueMapper::selectByExample)
        .filter(StringUtil::isNotEmpty)
        .ifPresent(clues -&gt; {
                    final ClueResp clueResp = Optional.of(req)
                            .filter(c -&gt; {
                                c.count = clues.size();
                                return true;
                            })
                            .map(this::awardValue)
                            .orElseThrow(() -&gt; ExceptionUtil.createParamException("参数错误"));
                    final Integer specialId = req.getIsHead()
                            ? clues.get(0).getId()
                            : clues.get(clues.size() - 1).getId();
                    clues.stream()
                            .peek(clue -&gt; {
                                final AwardConfig awardConfigclone = Optional.of(awardConfig)
                                        .map(JSONUtil::obj2Json)
                                        .map(json -&gt; JSONUtil.json2Obj(json, AwardConfig.class))
                                        .orElseGet(AwardConfig::new);
                                awardConfigclone.setMoney(
                                        Optional.of(clue.getId())
                                                .filter(specialId::equals)
                                                .map(e -&gt; clueResp.specialReward.longValue())
                                                .orElse(clueResp.otherAverageReward.longValue())
                                );
                                eventActionService.assembleAward(
                                        awardConfigclone,
                                        clue.getAdviserUid(),
                                        clue.getAdviserUid(),
                                        clue.getClueUid(),
                                        eventAction,
                                        new PasMessageParam(),
                                        clue.getId(),
                                        AwardRelationType.Clud.code()
                                );
                            })
                            .forEach(clue -&gt; {
                                clue.setOrderNo(req.orderNo);
                                clue.setCommodityName(req.commodityName);
                                clue.setOrderAmount(req.orderAmount);
                                clue.setReceived(true);
                                clue.setModifyTime(now);
                                clueMapper.updateByPrimaryKeySelective(clue);
                            });
                }
        );</code></pre>

<p>Java 就是这么一门神奇的语言，任何水平的人都能写出可以运行的代码，但是一看代码便知水平高低。</p>

<p>但是这样的 Stream 代码你一定一口老谈不吐不快，请移步下面链接发表评论，领取奖品：</p>

<h4><a href="https://gitee.com/oschina/bullshit-codes/blob/master/java/NBStream.java">https://gitee.com/oschina/bullshit-codes/blob/master/java/NBStream.java</a></h4>

<p>码云 6 周年，我们正在征集各种坑爹代码，很多奖品等你来拿</p>

<p>详细的参与方法请看&nbsp;&nbsp;<a href="https://gitee.com/oschina/bullshit-codes">https://gitee.com/oschina/bullshit-codes</a></p>

<p>------ 分割线 ------</p>

<p>其他坑爹代码吐槽：</p>

<ul>
	<li><a href="https://www.oschina.net/news/107032/gitee-bullshit-codes-stringbuffer" target="_blank">坑爹代码&nbsp;| 这样使用 StringBuffer 的方法有什么坑？</a></li>
	<li><a href="https://www.oschina.net/news/107081/gitee-bad-code-one-line-code" target="_blank">坑爹代码&nbsp;| 你写过的最长的一行代码有多长？？？</a></li>
	<li><a href="https://www.oschina.net/news/107151/nesting-bad-code">坑爹代码 | 循环+条件判断，你最多能嵌套几层？</a></li>
	<li><a href="https://www.oschina.net/news/107213/gitee-bullshit-codes-optimize-for-future">坑爹代码 | 为了后期优化查询速度 ~ 颇有商业头脑！</a></li>
	<li><a href="https://www.oschina.net/news/107315/gitee-bad-code-exception">坑爹代码 | 你是如何被异常玩然后变成玩异常的？</a></li>
</ul>
