---
layout: post
title: "码云六周年系列更新第一弹 —— 企业知识库管理改版"
---

<p><strong>值此码云 6 周年之际，我们为大家准备一系列更新以及活动。活动请看&nbsp;<a href="https://oschina.gitee.io/6th/?utm_source=oschina-news">https://oschina.gitee.io/6th</a></strong></p>

<p>此为更新第一弹！</p>

<p>码云的企业知识库管理功能（文档协作）更新了。在码云企业版中，「<strong>文档」</strong>功能用于构建团队知识库，方便成员共享知识；文件夹包含文档及文件，为企业提供统一的文档管理。</p>

<p><img height="auto" src="https://static.oschina.net/uploads/img/201905/28083316_2Wrp.jpg" width="612px" /></p>

<p>在<strong>「文档」</strong>中，每位用户都拥有一个独立的知识管理空间，对文档的类别、发布、预览、历史版本有一个清晰的存储和回溯。</p>

<p><strong>这次，码云为了提升用户对知识库管理的体验，在文档原有的功能上强化了3个功能：</strong></p>

<h2>1、知识库整合</h2>

<p>将<strong>「企业文档」、「企业附件」、「仓库Wiki」</strong>整合在一个视图中，将知识库信息集中和有序规范化，便于随时查阅。</p>

<p><img height="auto" src="https://static.oschina.net/uploads/img/201905/28083316_HpFC.jpg" width="1288" /></p>

<h3>&nbsp;</h3>

<h2><strong>2、文件共享</strong></h2>

<p>支持多级文件夹，方便管理知识的类型及内容，让成员快速准确的获取信息，缩短检索时间。</p>

<p><img height="auto" src="https://static.oschina.net/uploads/img/201905/28083317_rFgj.jpg" width="1221" /></p>

<h2><strong>3、更细的文档权限管理</strong></h2>

<p>可按角色、文件夹、文档来独立配置成员访问权限：「无权限」、「只读」、「读写」，帮助管理者进行知识产权保护，降低代码外泄风险。</p>

<p><img height="auto" src="https://static.oschina.net/uploads/img/201905/28083317_XhbD.jpg" width="1855" /></p>

<p><img height="auto" src="https://static.oschina.net/uploads/img/201905/28083317_bokF.jpg" width="722" /></p>

<p>此外，此次更新对企业级的文档以及项目级的文档进行隔离，结构更加清晰。</p>

<p>知识点非常多，欢迎大家探索：）</p>

<p>更多关于码云企业版的介绍请看&nbsp;<a href="https://gitee.com/enterprises" target="_blank">https://gitee.com/enterprises</a></p>
