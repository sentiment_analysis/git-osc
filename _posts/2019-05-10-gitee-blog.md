---
layout: post
title: "码云之 SVN 增加 SSH 支持，SVN 免密推拉代码"
---

<p>早在 2015 年，码云就已经支持 SVN 访问，是国内首个支持 SVN 访问的代码托管平台。经过 4 年多的不断改进（当然主要是解决协议适配的各种小问题）已比较稳定。</p>

<p><img alt="" src="https://static.oschina.net/uploads/img/201905/10074903_ow2V.png" width="400" /></p>

<p>近期我们的 SVN 模块又上线了对 SSH 的支持，也就是可以通过 svn+ssh 的方式访问代码仓库。使用 SSH 的好处有两点：</p>

<ol>
	<li>更安全</li>
	<li>支持免密推拉代码（使用证书）</li>
</ol>

<p>使用方法：</p>

<p>码云的仓库默认是不开启 SVN 访问支持的，需要在项目的管理界面中开启 SVN 访问，配置项如下图：</p>

<p><img src="https://oscimg.oschina.net/oscnet/4463750fbf8cb2483e1b23151791314ac9c.jpg" width="500" /></p>

<p>开启完 SVN 访问后就可以通过以下两种方式来访问，以 <a href="https://gitee.com/ld/J2Cache">J2Cache</a> 项目为例：</p>

<ol>
	<li>&nbsp;svn://gitee.com/ld/J2Cache</li>
	<li>&nbsp;svn+ssh://gitee.com/ld/J2Cache</li>
</ol>

<p>如下图：</p>

<p><img src="https://static.oschina.net/uploads/space/2019/0510/093625_qhoB_12.png" width="400" /></p>

<p>其中第一种方式是普通的 SVN 操作地址，不支持加密，需要提供码云的账号和密码来提交代码。</p>

<p>而第二种方式结合了&nbsp;SVN 和 SSH ，在使用之前需要先配置好证书，配置方法请看&nbsp;<a href="https://gitee.com/help/articles/4191">帮助文档</a>。这样就可以实现既安全又免密的代码推拉。</p>

<p>（请注意，使用 svn+ssh 的方式，不管是推拉代码，都要求开发者必须是仓库开发者成员）</p>

<p>------------------</p>

<p>最后，由于码云的 SVN 支持，其底层还是 Git 的存储，因此码云的 SVN 并不能完全等同于 SVN，它实现的是 Git 和 SVN 的功能重叠区域。我们提供 SVN 支持的初衷并非要替代已有的 SVN 服务，而是为了给 SVN 的开发者在往 Git 迁移的过程中提供一个平滑过渡的方案。</p>

<p><img alt="" src="https://static.oschina.net/uploads/img/201905/10075230_HhaB.png" width="400" /></p>

<p>所以，推荐使用 Git ！</p>

<p>使用 Git 就上码云 <a href="https://gitee.com">https://gitee.com</a></p>
